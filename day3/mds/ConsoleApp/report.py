import numpy as np
import pandas as pd
from databaseConnector import getDBConnection as connection 

def generateCustomerCSV():
	db,cursor=connection()
	dataframe=pd.read_sql('select * from customer',db)
	dataframe.to_csv("report/customer.csv")

# generateCustomerCSV()
def generateProductCSV():
	db,cursor=connection()
	dataframe=pd.read_sql('select * from product',db)
	dataframe.to_csv("report/product.csv")

def totalOrderedCSV():
	db,cursor=connection()
	dataframe=pd.read_sql('select p.product_name,p.product_price,p.product_quantity,b.buying_quantity,b.buy_date,c.customer_name from buying as b inner join product as p on b.product_id = p.product_id inner join customer as c on c.customer_id = b.customer_id',db)
	dataframe.to_csv("report/total_ordered.csv")

