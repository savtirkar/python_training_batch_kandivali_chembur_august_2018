from customer import loginCustomer,getCustomer,registerCustomer
from product import getAllProduct,registerProduct,getProductDetail,getProductPrice
from admin import registerAdmin,checkLogin
from buying import BuyProduct,getTotalBuyDetail
from datetime import datetime
from report import generateCustomerCSV,generateProductCSV,totalOrderedCSV
customer_login_id=[]
def checkCustomerCredential():
    customer_name=input("Enter customer name!!")    
    customer_contact_no=input("Enter customer Contact No!!")
     
    if(loginCustomer(customer_name,customer_contact_no)):
        customer_login_id.append(getCustomer(customer_name,customer_contact_no)[0][0])
        return True
    else:
        return False

def checkAdminCredential():
    username=input("Enter your username")
    password = input("Enter your password")
    return checkLogin(username,password)

def customerArena(choice):
    if(choice==1):
        products=getAllProduct()
        for product in products:
           print()
           print("###################")
           print(f"Product ID :- {product[0]}")
           print(f"Product Name :- {product[1]}")
           print(f"Product Price :- {product[2]}")
           print(f"Product Quantity :- {product[3]}")
           print("####################")
           print()
        product_id=int(input("Enter product Id For Buying !! "))
        buying_quantity = int(input("Enter buying quantity !!"))
        total_bill = float(getProductPrice(product_id))*buying_quantity
        print("#######################")
        print(f"Total Quantity of Product id {product_id} is {buying_quantity}")
        print(f"Total Cost of Your is : - {total_bill}")
        print("#######################")
        if(input("Confirm Your Order(y/n)")=="y"):
            print(BuyProduct(customer_login_id[0],product_id,buying_quantity,total_bill,datetime.now()))
    elif(choice==2):
        totalTransaction()

def adminArena(choice):
    try:
        if(choice==1):
            registerProductPanel()
        elif(choice ==2):
            registerCustomerPanel()
        elif(choice==3):
            generateReport()
        elif(choice==4):
            totalTransaction()
        elif(choice ==5):
            goodBy()
            
        else:
            print("Invalid Choice!!")    
    except BaseException as e:
        print("try again!!!")


def generateReport():
    choice=int(input("1.Customer Report 2.Product Report")) 
    if(choice==1):
        generateCustomerCSV()
        print("Kindly check Report Directory For Your Customer Report!!!")
    elif(choice==2):
        generateProductCSV()
        print("Kindly check Report Directory For Your Product Report!!!")
    else:
        print("Invalid Choice!!!")
def totalTransaction():
    totalOrderedCSV()
    print("your Total Transaction Report is Present at Report Directory")

def goodBy():
    print("thanks for using our service !!".center(100,"*"))
    

def registerProductPanel():
    name=input("Enter Product Name")
    price=input("Enter Product Price")
    quantity=input("Enter Product Quantity")
    print(registerProduct(name,price,quantity,datetime.now()))

def registerCustomerPanel():
    customer_name=input("Enter Name")
    customer_contact_no=input("Enter Customer Contact No")
    customer_address=input("Enter Address")
    customer_type=input("Enter Customer Type(S/I)")
    email = input("Enter Customer Email!")
    print(registerCustomer(customer_name,customer_contact_no,customer_address,customer_type,email,datetime.now()))

def run():
    print("Welcome to Milk Management System".center(100,"*"))
    while(True):
        print("Choice following options carefully!!")
        user_choice=int(input("1.Customer 2.Admin 3.exit"))
        if(user_choice==1):
            if(checkCustomerCredential()):
                functionality_choice = int(input("1.Buy Product  2.Ordered Product 3.Logout"))
                customerArena(functionality_choice)
            else:
                print("Wrong Credential Try Again!!!")
                if(input("Do You want to register(y/n)")=="y"):
                    registerCustomerPanel()
        elif(user_choice==2):
            if(checkAdminCredential()):
                functionality_choice = int(input("1.Add Product 2.Add Customer 3.Generate Report 4.Total Transaction 5.Logout"))
                adminArena(functionality_choice)
            else:
                print("Wrong Credential Try Again!!!")
              
        else:
            print("Thanks For using our Service !!!!".center(100,"*"))
            break

if __name__ == '__main__':
    run()








