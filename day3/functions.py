# functional programming

###########
#type 1 
#function without parameter
#declaring function
# def printMe():
# 	print("Hello iam in function!!")
# # calling function
# printMe()
# # assigning function
# a  = printMe
# a()
# print(type(a))
############
#type 2 
# function with parameter
# defining function 
def calculator(a,b):
	print(f"sum is :- {a+b} multiple is :- {a*b} division  is :- {a/b} and subtract is :- {a-b}")

# calling function 
# calculator(10,5)
# calculator()
#############
# type 3 
# function with default values
def calculator(a=1,b=1):
	print(f"sum is :- {a+b} multiple is :- {a*b} division  is :- {a/b} and subtract is :- {a-b}")

# calling function 
# calculator()
# calculator(5)
# calculator(10,5)
# calculator(a=10,b=5)
# calculator(b=10,a=5)
# calculator(10,)
# calculator(,10) #-->raise syntax error 
# calculator(x=10,y=6) # --> raise exception x and y are unknown
#################
# type 4 --> function with return 
def square(a):
	return(a*a)

def getRoot(no,root = 2):
	return(no**(1/root))
# calling
# print(square(4))
# print(getRoot(4))
# print(getRoot(25,root = 3))
##################
# type 5 
# function with multiple return

def getRaiseToAndRoot(no,root=2):
	return(no**no,no**(1/root))

# # calling 
# raiseTo,root =getRaiseToAndRoot(10,3)
# print(raiseTo)
# print(root)

###################
# type 6
# function with args
# declaring function 

def getArgs(*args):
	print(args)
	for i in args:
		print(i)

# calling functions
# getArgs(23,"hello",[1,2,3,4],(2,43,5,4))
# getArgs(23.45)
####################
# type 7 
# function with kwargs
# declaring of kwargs

def iamWithKwargs(**kw):
	print(kw)

# calling of function 
# iamWithKwargs(name = "lokesh",std="BE",roll_no=36,courses=["java,python,php"])

# iamWithKwargs(1="sion",2="cst",3="dadar")
# numbersystem is not allow as key using kwargs
###################
# type 8
# function with arg and kwargs
# function declaration 
def argsWithKwargs(*a,**b):
	print(a,b)
#calling function 
# argsWithKwargs(23,34,1,"hello",name = "lokesh",dob = "december")
####################
# type 9 
# lambda function 
# Anonynous function
# single line function 
# lambda parameter : expression
square=lambda x:x*x
# print(square(10))
inc = lambda x: x+1
dec = lambda x:x-1
# print(inc(34))
# print(dec(34))
greater = lambda x,y:x if(x>y) else y
# print(greater(10,20))
greater = lambda *args :max(args)
# print(greater(12,3,4,5,6))
# print(greater("man","can","van"))
###################
# type 10 
# filter(),map(),reduce()
# map(func,data_source)
list1 = list(range(1,10))
# print(list(map(lambda x:x*x,list1)))
# print(list(map(lambda x:x*x+x+1,list1 )))
# print(list(map(lambda x:x*x*x+x*x+x+1,list1 )))

# print(list(filter(lambda x:x%2!=0,list1)))

list2 =["goa","MAHARASHTRA","KasHmir","daman"]

def checkCase(x):
	if(x.islower()):
		return x.upper()
	elif(x.isupper()):
		return x.lower()
	else:
		return x.swapcase()

def filterUpper(x):
	if(x.isupper()):
		return True


# print(list(map(lambda x:checkCase(x),list2)))
# print(list(filter(lambda x:filterUpper(x),list2)))
####################
# reduce function 

from functools import reduce as r
list1 = list(range(1,10))
sums=r(lambda x,y:x+y,list1)
avg  = sums/len(list1)
# print(avg)
# print(r(lambda x,y:(x*x+x*y+y*y),list1))
####################
# decorators
# first class citizen function
# closure


def wrapper(func):
	def inner():
		print("before func execution!!")
		func()
		print("after func execution")
	return inner

@wrapper # printMe = wrapper(printMe)
def printMe():
	print("hello all !! nice to meet me")
# printMe()
@wrapper
def demo():
	print("demo print")
# demo()
#################


































