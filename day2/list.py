# CRUD Operation on list
#CREATE
list1 = list(range(1,10))
list1 = [x for x in range(1,10)]
##############################
#READ
print(list1)
print()
print()
## 
# print(list1[1:5])
# print(list1[::-1])
# print(list1[9:2])
# print(list1[9:2:-1])
# print(list1[-2:-6:-2])
# print(list1[6:-6:-2])
# print(list1[::-1][::1][::2][::3][0])
# print(list1[1::1][2::2][3::3])

############################

# Update
#scenario 1
# override
# list1[2]="red"
# print(list1)

#scenario 2
# list1[2]=["red","green"]
# print(list1)

#scenario 3
# list1[2:3]=["red","green"]
# print(list1)
# list1[1:3]=["red","green","black"]
# print(list1)

#scenario 4
# list1[1:5]=["red","green"]
# print(list1)


##################
# delete
# list1[2]=["red","green"]
# print(list1)

# del list1[2][1]
# print(list1)
################## 
# function and methods


















