# if Statement
# a = 20
# b = 40
# c = 60

# if(a>b and a>c):
# 	print("a is greater")
# elif(b>a and b>c):
# 	print("b is greater")
# else:
# 	print("c is greater")

#single line if else statement
# print("a is greater") if(a>b and a>c) else print("b is greater") if(b>a and b>c) else print("c is greater")

# take no from user divisible by 2 or 3 or both
# no = float(input("Enter no"))
# if(no%2==0 and no%3==0):
# 	print(f" {no} is divisible by 2 and 3")
# elif(no%2==0):
# 	print(f" {no} is divisible by 2")
# elif(no%3==0):
# 	print(f" {no} is divisible by 3")
# else:
# 	print(f" {no} is not divisible by 2 or 3 or both")






